import React from 'react';
import './App.css';
import {Button, Typography } from 'antd'
import {PlusOutlined } from '@ant-design/icons'

const {Title, Text, Paragraph} = Typography

function App() {
  return (
    <div className="App">
     <Title level={2}>Hello world!</Title>
     <Text>This is a text</Text>
     <Paragraph copyable>Copyable Text</Paragraph>
     <br />
     <Button type='primary' size='small'><PlusOutlined /> Create new record </Button>
    </div>
  );
}

export default App;
